﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMap : MonoBehaviour {

    [SerializeField]
    GameObject column;
    [SerializeField]
    GameObject cell;
    [SerializeField]
    float animationTime = 1;

    Element[][] matrix;
    UIMapElement[][] mapElements;

	void Start ()
    {
        if (MapController.Instance != null)
            ShowMap();
        MapController.OnMapCreated += ShowMap;
        MapController.OnMapChanged += ChangeMap;
	}

    private void OnDestroy()
    {
        MapController.OnMapCreated -= ShowMap;
        MapController.OnMapChanged -= ChangeMap;
    }

    void ShowMap()
    {
        matrix = new Element[MapController.Instance.Map.Length][];
        mapElements = new UIMapElement[matrix.Length][];
        for (var i = 0; i < matrix.Length; i++)
        {
            GameObject col = Instantiate(column, transform);
            matrix[i] = new Element[MapController.Instance.Map[i].Length];
            mapElements[i] = new UIMapElement[matrix[i].Length];
            for (var j = 0; j < matrix[i].Length; j++)
            {
                matrix[i][j] = MapController.Instance.Map[i][j];
                GameObject cel = Instantiate(cell, col.transform);
                var elem = cel.GetComponent<UIMapElement>();
                if (elem == null)
                {
                    Debug.LogError("Missing script UIMapElement.");
                    continue;
                }
                elem.SetPicture(matrix[i][j], new Vector2Int(i, j));
                mapElements[i][j] = elem;
            }
        }
        StartCoroutine(anima());
    }

    void ChangeMap()
    {
        for (var i = 0; i < matrix.Length; i++)
        {
            for (var j = 0; j < matrix[i].Length; j++)
            {
                if (matrix[i][j] != MapController.Instance.Map[i][j])
                {
                    matrix[i][j] = MapController.Instance.Map[i][j];
                    mapElements[i][j].SetPicture(matrix[i][j], new Vector2Int(i, j));
                }
            }
        }
        StartCoroutine(anima());
    }

    IEnumerator anima()
    {
        float tm = 0;
        while (tm < animationTime)
        {
            tm += Time.deltaTime;
            yield return null;
        }
        MapController.Instance.CheckCombinations();
    }
}
