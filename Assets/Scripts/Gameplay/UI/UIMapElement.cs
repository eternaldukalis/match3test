﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMapElement : MonoBehaviour {

    [SerializeField]
    Toggle toggle;

    GameObject pic;
    Element curElement;
    Vector2Int position;

	void Start ()
    {
        MapController.OnSelectionCleared += ClearSelection;
	}

    private void OnDestroy()
    {
        MapController.OnSelectionCleared -= ClearSelection;
    }

    public void SetPicture(Element element, Vector2Int position)
    {
        if (pic != null)
        {
            ElementsPoolsManager.Instance.ReleaseElementObject(curElement, pic);
        }
        this.position = position;
        pic = ElementsPoolsManager.Instance.TakeElementObject(element);
        pic.transform.SetParent(transform, false);
        pic.transform.SetAsFirstSibling();
        curElement = element;
    }

    public void Switch()
    {
        if (toggle.isOn)
            MapController.Instance.Select(position);
        else
            MapController.Instance.Unselect(position);
    }

    void ClearSelection()
    {
        toggle.isOn = false;
    }
}
