﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMapGenerator
{
    Element[][] Generate(Element[][] source, MapParameters parameters);
}

public class MapGenerator : IMapGenerator
{
    public Element[][] Generate(Element[][] source, MapParameters parameters)
    {
        Element[][] Matrix = source;
        for (var i = 0; i < Matrix.Length; i++)
        {
            for (var j = 0; j < Matrix[i].Length; j++)
            {
                if (Matrix[i][j] == Element.Null)
                    Matrix[i][j] = parameters.TypeRandomizationMethod.Randomize();
            }
        }
        return Matrix;
    }
}
