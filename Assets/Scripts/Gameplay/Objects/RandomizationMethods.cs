﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRandomizationMethod<T>
{
    T Randomize();
}

public class EqualRandomization<T> : IRandomizationMethod<T>
{
    public T[] Array { private set; get; }
    public EqualRandomization(T[] array)
    {
        Array = array;
    }
    public T Randomize()
    {
        int index = Random.Range(0, Array.Length);
        return Array[index];
    }
}