﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IElementsMover
{
    Element[][] Move(Element[][] source);
}

public class DoingNothingMover : IElementsMover
{
    public Element[][] Move(Element[][] source)
    {
        return source;
    }
}

public class DownMover : IElementsMover
{
    public Element[][] Move(Element[][] source)
    {
        Element[][] res = source.Clone() as Element[][];
        for (var i = 0; i < res.Length; i++)
        {
            for (var j = res[i].Length - 1; j >= 0; j--)
            {
                if (res[i][j] == Element.Null)
                {
                    for (var k = j - 1; k >= 0; k--)
                    {
                        if (res[i][k] != Element.Null)
                        {
                            res[i][j] = res[i][k];
                            res[i][k] = Element.Null;
                            break;
                        }
                    }
                }
            }
        }
        return res;
    }
}
