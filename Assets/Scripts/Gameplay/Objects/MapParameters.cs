﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapParameters
{
    public int Width { get; set; }
    public int Height { get; set; }
    public IRandomizationMethod<Element> TypeRandomizationMethod { get; set; }
    public Dictionary<Vector2Int, Element> PredeterminedElements { get; set; }
    public IElementsMover ElementsMover { get; set; }
}