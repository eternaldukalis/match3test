﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsPool : MonoBehaviour {

    public int Size { private set; get; }

    GameObject parent;

    Stack<GameObject> stack;
    Element element;

	void Start ()
    {
		
	}

    public void Create(int size, Element element, GameObject parent)
    {
        this.element = element;
        this.parent = parent;
        Size = size;
        stack = new Stack<GameObject>();
        while (stack.Count < Size)
        {
            stack.Push(Load());
        }
    }

    public GameObject Take()
    {
        if (stack.Count == 0)
        {
            stack.Push(Load());
            Size++;
        }
        return stack.Pop();
    }

    public void Release(GameObject obj)
    {
        stack.Push(obj);
        obj.transform.SetParent(parent.transform, false);
    }

    GameObject Load()
    {
        GameObject res = ElementsLoader.Instance.LoadObject(element);
        res.transform.SetParent(parent.transform, false);
        return res;
    }
}
