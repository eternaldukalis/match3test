﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsPoolsManager : MonoBehaviour {

    [SerializeField]
    int size = 20;
    [SerializeField]
    GameObject parent;
    Dictionary<Element, ElementsPool> pools;

    public static ElementsPoolsManager Instance { private set; get; }

	void Start ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;

        pools = new Dictionary<Element, ElementsPool>();
        for (var i = 1; i < Enum.GetValues(typeof(Element)).Length; i++)
        {
            var pool = gameObject.AddComponent<ElementsPool>();
            pool.Create(size, (Element)i, parent);
            pools.Add((Element)i, pool);
        }
	}

    public GameObject TakeElementObject(Element element)
    {
        return pools[element].Take();
    }

    public void ReleaseElementObject(Element element, GameObject obj)
    {
        pools[element].Release(obj);
    }
}
