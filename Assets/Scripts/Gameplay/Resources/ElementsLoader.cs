﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsLoader : MonoBehaviour {

    public static ElementsLoader Instance { private set; get; }

    [SerializeField]
    string elementsPath = "Prefabs/Map/Elements/";
    [SerializeField]
    ElementPathPair[] paths;

	void Awake ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;
	}

    public GameObject LoadObject(Element element)
    {
        GameObject res = null;
        foreach (var x in paths)
        {
            if (x.Element == element)
            {
                var obj = Resources.Load<GameObject>(elementsPath + x.Path);
                res = Instantiate(obj);
                break;
            }
        }
        if (res == null)
        {
            Debug.LogError("Wrong element type or path.");
        }
        return res;
    }
}

[System.Serializable]
public struct ElementPathPair
{
    [SerializeField]
    public Element Element;
    [SerializeField]
    public string Path;
}