﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Element { Null, Red, Green, Blue, Yellow }

public class MapController : MonoBehaviour {

    public static MapController Instance { private set; get; }

    public static event System.Action OnMapCreated;
    public static event System.Action OnMapChanged;
    public static event System.Action OnSelectionCleared;

    public Element[][] Map { private set; get; }

    [SerializeField]
    Combination[] combinations;

    IMapGenerator generator;
    MapParameters parameters;
    List<Vector2Int> selected;

	void Start ()
    {
        if (Instance != null)
            Destroy(Instance);
        Instance = this;

        // Test map creation.
        Element[] elements = { Element.Blue, Element.Green, Element.Red, Element.Yellow };
        var parameters = new MapParameters
        {
            Width = 10,
            Height = 8,
            TypeRandomizationMethod = new EqualRandomization<Element>(elements),
            PredeterminedElements = new Dictionary<Vector2Int, Element>(),
            ElementsMover = new DownMover(),
        };
        var generator = new MapGenerator();
        Create(parameters, generator);
	}

    public void Create(MapParameters parameters, IMapGenerator mapGenerator)
    {
        this.parameters = parameters;
        generator = mapGenerator;
        selected = new List<Vector2Int>();
        Map = new Element[parameters.Width][];
        for (var i = 0; i < Map.Length; i++)
            Map[i] = new Element[parameters.Height];
        if (parameters.PredeterminedElements != null)
        {
            foreach (var x in parameters.PredeterminedElements)
            {
                try
                {
                    Map[x.Key.x][x.Key.y] = x.Value;
                }
                catch
                {
                    Debug.LogError("Map bounds violation.");
                }
            }
        }
        mapGenerator.Generate(Map, parameters);
        if (OnMapCreated != null)
            OnMapCreated();
        CheckCombinations();
        
    }

    public void Select(Vector2Int position)
    {
        selected.Add(position);
        if (selected.Count == 2)
        {
            int dist = Mathf.Abs(selected[0].x - selected[1].x) + Mathf.Abs(selected[0].y - selected[1].y);
            if (dist == 1)
            {
                Swap(selected[0], selected[1]);
            }
            selected.Clear();
            if (OnSelectionCleared != null)
                OnSelectionCleared();
        }
    }

    public void Unselect(Vector2Int position)
    {
        selected.Remove(position);
    }

    public void CheckCombinations()
    {
        List<Vector2Int> comb = FindCombination();
        if (comb.Count == 0)
            return;
        foreach (var x in comb)
        {
            Map[x.x][x.y] = Element.Null;
        }
        parameters.ElementsMover.Move(Map);
        generator.Generate(Map, parameters);
        if (OnMapChanged != null)
            OnMapChanged();
        //CheckCombinations();
    }

    void Swap(Vector2Int pos1, Vector2Int pos2)
    {
        Element res = Map[pos1.x][pos1.y];
        Map[pos1.x][pos1.y] = Map[pos2.x][pos2.y];
        Map[pos2.x][pos2.y] = res;
        if (OnMapChanged != null)
            OnMapChanged();
        CheckCombinations();
    }

    List<Vector2Int> FindCombination()
    {
        var res = new List<Vector2Int>();
        int priority = -1;
        for (var i = 0; i < Map.Length; i++)
        {
            for (var j = 0; j < Map[i].Length; j++)
            {
                for (var k = 0; k < combinations.Length; k++)
                {
                    if (combinations[k].Priority >= priority)
                    {
                        var curres = new List<Vector2Int>();
                        curres.Add(new Vector2Int(i, j));
                        bool passed = true;
                        foreach (var x in combinations[k].Matched)
                        {
                            int pointx = i + x.x;
                            int pointy = j + x.y;
                            if ((pointx >= Map.Length) || (pointy >= Map[i].Length) || (Map[i][j] != Map[pointx][pointy]))
                            {
                                passed = false;
                                break;
                            }
                            curres.Add(new Vector2Int(pointx , pointy));
                        }
                        if (passed)
                        {
                            res = curres;
                            priority = combinations[k].Priority;
                        }
                    }
                }
            }
        }
        return res;
    }
}

[System.Serializable]
public struct Combination
{
    [SerializeField]
    public List<Vector2Int> Matched;
    [SerializeField]
    public int Priority;
}
